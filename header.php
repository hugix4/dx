 <!DOCTYPE html>
  <html>
    <head>
      <link rel="icon" href="images/favicon2.png" type="image/png">
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <style type="text/css"> body {
    display: flex;
    min-height: 100vh;
    flex-direction: column;
  }

  main {
    flex: 1 0 auto;
  }</style>

    <body>

      <ul id="productos" class="dropdown-content">
        <li><a href="cupcakes.php">Cupcakes</a></li>
        <li><a href="galletas.php">Galletas</a></li>
        <li><a href="pasteles.php">Pasteles</a></li>
        <li><a href="mamutsybubulubus.php">Mamuts y Bubulubus</a></li>
        <li><a href="manzanas.php">Manzanas Decoradas</a></li>
      </ul>

      <ul id="productos_m" class="dropdown-content">
        <li><a href="cupcakes.php">Cupcakes</a></li>
        <li><a href="galletas.php">Galletas</a></li>
        <li><a href="pasteles.php">Pasteles</a></li>
        <li><a href="mamutsybubulubus.php">Mamuts y Bubulubus</a></li>
        <li><a href="manzanas.php">Manzanas Decoradas</a></li>
      </ul>

      <nav>
        <div class="nav-wrapper">
          <a href="index.php" class="brand-logo-leftt"><img src="images/logodx.png" width="70px"></a>          
          <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
          <ul class="right hide-on-med-and-down">
            <li><a href="index.php">Inicio</a></li>
            <li><a href="quienes-somos.php">Quienes Somos</a></li>
            <li><a class="dropdown-button" href="#!" data-activates="productos">Nuestros Productos<i class="material-icons right">arrow_drop_down</i></a></li>
            <li><a href="blog.php">Blog</a></li>
            <li><a href="contacto.php">Contacto</a></li>
            <li><a href="faqs.php">FAQS</a></li>
          </ul>
          <ul class="side-nav" id="mobile-demo">
            <a href="index.php" style="text-align: center;"><img src="images/logodx.png" width="120px"></a>            
            <li><a href="index.php">Inicio</a></li>
            <li><a href="quienes-somos.php">Quienes Somos</a></li>
            <li><a class="dropdown-button" href="#!" data-activates="productos_m">Nuestros Productos<i class="material-icons right">arrow_drop_down</i></a></li>
            <li><a href="blog.php">Blog</a></li>
            <li><a href="contacto.php">Contacto</a></li>
            <li><a href="faqs.php">FAQS</a></li>
          </ul>
        </div>
      </nav>
