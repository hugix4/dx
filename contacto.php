<?php include 'header.php';
include 'textos.php';
include 'modals.php';
include 'boximg.php';
?>
  <main>

  <div class="container">
<!-- Modal Trigger -->
      <div class="col s4 m4">
        <div class="card-panel blue">
        <table class="table">
          <th colspan="2" style="text-align: center;">
            <span class="white-text">Contáctanos por teléfono, sms,  Whatsapp o en nuestras ﻿redes sociales﻿:</span>
          </th>
          <tr>
            <td style="text-align: center;">
              <img src="images/whatsapp.png" width="20%">
            </td>
            <td>
              <span style="font-weight: bold; color: white; margin-bottom: 10px;">55 31 29 18 36</span>
            </td>
          </tr>

          <tr>
            <td style="text-align: center;">
              <a href="https://www.facebook.com/DulcXoorpresas"><img src="images/fb.png" width="20%"></a>
            </td>
            <td>
              <a href="https://www.facebook.com/DulcXoorpresas"><span style="font-weight: bold; color: white; margin-bottom: 10px;">@DulcXoorpresas</span></a>
            </td>
          </tr>

          <tr>
            <td style="text-align: center;">
              <a href="https://www.instagram.com/dulcesxoorpresas"><img src="images/instagram.png" width="20%"></a>
            </td>
            <td>
              <a href="https://www.instagram.com/dulcesxoorpresas"><span style="font-weight: bold; color: white;">@dulcesxoorpresas</span></a>
            </td>
          </tr>

          <tr  >
            <td colspan="2" style="text-align: center;">
              <span class="white-text">O bien, escribenos a: <a class="white-text" href="mailto:dulcesxoorpresas@gmail.com">dulcesxoorpresas@gmail.com</a></span>
            </td>
          </tr>

        </table>


        </div>
      </div>
</div>

  </main>
  <?php include 'footer.php';?>

  <script type="text/javascript">
    
  $(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();
  });
          
  </script>