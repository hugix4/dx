<?php
function boximg($img,$caption,$titulo,$idmodal,$hiperlink){
	echo '
	<div class="col s3 m3">
      <img class="materialboxed" width="100px" src="images/'.$img.'" data-caption="'.$caption.'">
      '.$titulo.' &nbsp;<a href="#'.$idmodal.'">'.$hiperlink.'</a>  
    </div>
  ';

}

function galleta_img($img,$caption){
	echo '
	<div class="col s3 m3">
      <img class="materialboxed" width="150px" height="150px" src="images/galletas/'.$img.'.jpg" data-caption="'.$caption.'">  
    </div>
  ';

}

function cupcake_img($img,$caption){
  echo '
  <div class="col s3 m3">
      <img class="materialboxed" width="150px" height="150px" src="images/cupcake/'.$img.'.jpg" data-caption="'.$caption.'">  
    </div>
  ';

}

function cake_img($img,$caption){
  echo '
  <div class="col s3 m3">
      <img class="materialboxed" width="150px" height="150px" src="images/cakes/'.$img.'.jpg" data-caption="'.$caption.'">  
    </div>
  ';

}

function myb_img($img,$caption){
  echo '
  <div class="col s3 m3">
      <img class="materialboxed" width="150px" height="150px" src="images/myb/'.$img.'.jpg" data-caption="'.$caption.'">  
    </div>
  ';
}

function manzana_img($img,$caption){
  echo '
  <div class="col s3 m3">
      <img class="materialboxed" width="150px" height="150px" src="images/manzanas/'.$img.'.jpg" data-caption="'.$caption.'">  
    </div>
  ';
}

?>
