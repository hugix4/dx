<?php include 'header.php';
include 'textos.php';
include 'modals.php';
include 'boximg.php';?>
<div class="parallax-container">
  <div class="parallax">
    <img src="images/galletas/g28.jpg">
  </div>
  <div class="caption center-align ">
    <br><br><br><br><br>
      <div style="background: rgba(0, 0, 0, 0.6);">
        <div>
          <h1 style="color: #ffee58;">Galletas</h1>
        </div>
      </div>
    </div>
</div>
  <br>

  <div class="container">
  <br><br>
    <div class="row">
      <?php
        galleta_img('g1','galletas');
        galleta_img('g2','galletas');
        galleta_img('g37','galletas');
        galleta_img('g11','galletas');
      ?>
    </div>

    <div class="row">
      <?php
        galleta_img('g14','galletas');
        galleta_img('g15','galletas');
        galleta_img('g20','galletas');
        galleta_img('g21','galletas');
      ?>
    </div>

    <div class="row">
      <?php
        galleta_img('g38','galletas');
        galleta_img('g40','galletas');
        galleta_img('g41','galletas');
        galleta_img('g42','galletas');
      ?>
    </div>

    <div class="row">
      <?php
        galleta_img('g26','galletas');
        galleta_img('g31','galletas');
        galleta_img('g32','galletas');
        galleta_img('g34','galletas');
      ?>
    </div>

    <div class="row">
      <?php
        echo '<div class="col s3 m3"></div>';
        galleta_img('g43','galletas');
        galleta_img('g44','galletas');
        galleta_img('g45','galletas');
        //echo '<div class="col s3 m3"></div>';
      ?>
    </div>

  </div>


  <div class="section green">
    <div class="row">
      <ul class="collapsible popout" data-collapsible="accordion">

        <li>
          <div class="collapsible-header"><i class="material-icons">android</i>Calaveritas / Halloween</div>
          <div class="collapsible-body">
            <div class="row">
              <?php
                    //$imagen,$texto en la imagen,$titulo,$idmodal
                    galleta_img('g6','galletas');
                    galleta_img('g7','galletas');
                    galleta_img('g8','galletas');
                    galleta_img('g9','galletas'); ?>
            </div><!--row-->
            <div class="row">
              <?php
                    //$imagen,$texto en la imagen,$titulo,$idmodal
                    echo '<div class="col s3 m3"></div>';
                    galleta_img('g22','galletas');?>
            </div><!--row-->
          </div><!--collapsible-->
        </li>

        <li>
          <div class="collapsible-header"><i class="material-icons">redeem</i>Navidad</div>
          <div class="collapsible-body">
            <div class="row">
              <?php
                    //$imagen,$texto en la imagen,$titulo,$idmodal
                    galleta_img('g19','galletas');
                    galleta_img('g5','galletas');
                    galleta_img('g3','galletas');
                    galleta_img('g4','galletas'); ?>
            </div><!--row-->
            <div class="row">
              <?php
                    //$imagen,$texto en la imagen,$titulo,$idmodal
                    echo '<div class="col s3 m3"></div>';
                    galleta_img('g35','galletas');?>
            </div><!--row-->
          </div><!--collapsible-->
        </li>

        <li>
          <div class="collapsible-header"><i class="material-icons">swap_horiz</i>Otros (Personajes)</div>
          <div class="collapsible-body">
            <div class="row" style="text-align: center;">
              <?php
                    //$imagen,$texto en la imagen,$titulo,$idmodal
                    galleta_img('g23','galletas');
                    galleta_img('g24','galletas');
                    galleta_img('g17','galletas');
                    galleta_img('g25','galletas'); ?>
            </div><!--row-->
            <div class="row" style="text-align: center;">
              <?php
                    //$imagen,$texto en la imagen,$titulo,$idmodal
                    galleta_img('g27','galletas');
                    galleta_img('g28','galletas');
                    galleta_img('g29','galletas');
                    galleta_img('g30','galletas'); ?>
            </div><!--row-->
            <div class="row" style="text-align: center;">
              <?php
                    //$imagen,$texto en la imagen,$titulo,$idmodal
                    galleta_img('g33','galletas');
                    galleta_img('g39','galletas');
                    galleta_img('g12','galletas');
                    galleta_img('g13','galletas');?>
            </div><!--row-->

            <div class="row" style="text-align: center;">
              <?php
                    //$imagen,$texto en la imagen,$titulo,$idmodal
                    echo '<div class="col s3 m3"></div>';
                    galleta_img('g18','galletas');
                    echo '<div class="col s3 m3"></div>';
                    echo '<div class="col s3 m3"></div>';?>
            </div><!--row-->

          </div><!--collapsible-->
        </li>

      </ul>

    </div>
  </div>
  <div class="parallax-container">
    <div class="parallax"><img src="images/266.jpg"></div>
  </div>
  <?php include 'footer.php';?>

  <script type="text/javascript">
    $(document).ready(function(){
      $('.parallax').parallax();
      $(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();
  });
    });



  </script>
