<?php include 'header.php';?>
<main>
	<div class="container">
		
		
  <!--div class="carousel">
    <a class="carousel-item" href="#one!"><img src="http://lorempixel.com/250/250/nature/1"></a>
    <a class="carousel-item" href="#two!"><img src="http://lorempixel.com/250/250/nature/2"></a>
    <a class="carousel-item" href="#three!"><img src="http://lorempixel.com/250/250/nature/3"></a>
    <a class="carousel-item" href="#four!"><img src="http://lorempixel.com/250/250/nature/4"></a>
    <a class="carousel-item" href="#five!"><img src="http://lorempixel.com/250/250/nature/5"></a>
  </div-->
      

	  	
	        
  		<br>
		<div class="slider">
		    <ul class="slides">
		      <li>
		        <img src="images/69.jpg"> <!-- random image -->
		        <div class="caption center-align">
		          <h3>Dulces Xoorpresas</h3>
		          <h5 class="light grey-text text-lighten-3">Pasteles</h5>
		        </div>
		      </li>
		      <li>
		        <img src="images/86.jpg"> <!-- random image -->
		        <div class="caption left-align">
		          <h3>Dulces Xoorpresas</h3>
		          <h5 class="light grey-text text-lighten-3">Cupcakes</h5>
		        </div>
		      </li>
		      <li>
		        <img src="images/142.jpg"> <!-- random image -->
		        <div class="caption right-align">
		          <h3>Dulces Xoorpresas</h3>
		          <h5 class="light grey-text text-lighten-3">Fondant</h5>
		        </div>
		      </li>
		      <li>
		        <img src="images/152.jpg"> <!-- random image -->
		        <div class="caption center-align">
		          <h3>Dulces Xoorpresas</h3>
		          <h5 class="light grey-text text-lighten-3">Galletas</h5>
		        </div>
		      </li>
		      <li>
		        <img src="images/245.jpg"> <!-- random image -->
		        <div class="caption center-align">
		          <h3>Dulces Xoorpresas</h3>
		          <h5 class="light grey-text text-lighten-3">Pasteles tematicos</h5>
		        </div>
		      </li>
		    </ul>
		  </div>

		  En Dulces Xoorpresas tenemos el complemento perfecto para tus celebraciones.
		  Revisa nuestros productos, si no encuentras lo que buscas nosotros lo realizamos.

	    <div class="row">
	      
	      <div class="col s6 m6">
	        <div class="card-panel blue">
	          <span class="white-text"><img src="images/DX1.jpg" width="100%">
	          </span>
	        </div>
	      </div>

	      <div class="col s6 m6">
	        <div class="card-panel blue">
	          <span class="white-text">Contáctanos por teléfono, sms,  Whatsapp o en nuestras ﻿redes sociales﻿:
				<br><br>Teléfono: 5531291836
				<br><br>
				<a href="https://www.facebook.com/DulcXoorpresas"><img src="images/fb.png" width="10%"><span style="font-weight: bold; color: white; margin-bottom: 10px;">@DulcXoorpresas</span></a>
				<br><br>
				<a href="https://www.instagram.com/dulcesxoorpresas"><img src="images/instagram.png" width="10%"><span style="font-weight: bold; color: white;">@dulcesxoorpresas</span></a>
				<br><br>Mail: dulcesxoorpresas@gmail.com
	          </span>
	        </div>
	      </div>

	      </div>

	      <div class="row">

	      <div class="col s12 m12">
	        <div class="card-panel blue">
	          <span class="white-text"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7529.350992506864!2d-99.2026431!3d19.33988260000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d20008e9511f59%3A0x14df829323044006!2sGuanajuato+8%2C+Progreso%2C+01080+Ciudad+de+M%C3%A9xico%2C+CDMX!5e0!3m2!1ses-419!2smx!4v1496718905198" width="100%" height="500px"  style="border:0" allowfullscreen></iframe>
	          </span>
	        </div>
	      </div>

	    </div>

	</div>
</main>
<?php include 'footer.php';?>

<script type="text/javascript">  
   /*$(document).ready(function(){
      $('.carousel').carousel();
    });*/

   $(document).ready(function(){
      $('.slider').slider();
    });

   /*$('.carousel.carousel-slider').carousel({fullWidth: true},setTimeout(autoplay, 3500));
    $('.carousel.carousel-slider').click(function(){
    	$('.carousel').carousel('next');	
    });
    function autoplay() {
      $('.carousel').carousel('next');
      setTimeout(autoplay, 3500);
    }*/ 

</script>