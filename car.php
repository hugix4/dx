<?php include 'header.php'; ?>

	

<div class="container">
	<div class="section">
		<div class="carousel carousel-slider" data-indicators="true">			
			<a class="carousel-item" href="#"><img src="images/69.jpg"></a>
			<a class="carousel-item" href="#"><img src="images/86.jpg"></a>
			<a class="carousel-item" href="#"><img src="images/142.jpg"></a>
		</div>
	</div>
</div>
<footer class="page-footer teal">
<?php include 'footer.php';?>

<script type="text/javascript">  
    $('.carousel.carousel-slider').carousel({fullWidth: true},setTimeout(autoplay, 3500));
    $('.carousel.carousel-slider').click(function(){
    	$('.carousel').carousel('next');	
    });
    function autoplay() {
      $('.carousel').carousel('next');
      setTimeout(autoplay, 3500);
    } 
</script>