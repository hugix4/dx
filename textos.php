<?php

	function img_centrada($ruta){
		$ruta_s = explode('/', $ruta);
		$ruta_t = explode('.', $ruta_s[1]);
	  return "<div class='container'>
	  <img class='materialboxed' width='100%' src='$ruta' alt='BG Solutions $ruta' title='BG Solutions $ruta_t[0]'>
	  </div>";
		//return "<span class='image featured' style = 'margin: auto; height: auto;'><img src='$ruta' alt='BG Solutions $ruta' title='BG Solutions $ruta_t[0]'/></span>";
	}

	function img_parallax($ruta,$texto, $porcentaje){
	  return "<div class='parallax-container valign-wrapper'>
	    <div class='section no-pad-bot'>
	      <div class='container'>
	        <div class='row center'>
	          <h5 class='header col s12 light'>$texto</h5>                    
	        </div>
	      </div>
	    </div>
	    <div class='parallax'><img src='$ruta' alt='BG Solutions $ruta' width='$porcentaje'></div>
	  </div>";
	}

	function titulo_negritas($titulo, $tamaño){
		return "<span style='font-weight: bold;'><h$tamaño>$titulo</h$tamaño></span>";
	}


	$quienes_somos1 = "<h4>¿Quiénes somos?</h4>

						Somos un negocio familiar que comenzó en el año 2012, haciendo galletas para baby shower, de ahí nació nuestro amor por la repostería, le siguieron los cupcakes, galletas y algunos pasteles. <br><br>
						Posteriormente nos adentramos en el mundo del fondant, tomando cursos en internet, asistiendo a talleres y clases en de repostería, hemos ido mejorando nuestra técnica.
					<br><br>

					Asi comenzó nuestra aventura, vendiendo nuestros productos principalmente a familiares y amigos cercanos, después nuestro mercado fue creciendo y gracias a nuestros clientes hemos adquirido más experiencia en cada uno de nuestros pedidos, sin duda alguna cada pedido ha tenido sus características particulares, con cada uno nos hemos divertido y tenido nuevas experiencias \"Xoorpresas\".
					<br><br>
					Nos encanta tomar cursos, talleres, platicas y asistir a exposiciones de repostería y decoración, en estas hemos podido conocer  nuevas técnicas e ideas para implementar y ofrecer a nuestros clientes. En Dulces Xoorpresas hacemos uso de la mejor materia prima a nuestro alcance para realizar nuestros trabajos.
					<br><br>
					Buscamos estar presentes en todas tus celebraciones y así contribuir a que tus invitados se lleven un recuerdo inolvidable con la Dulce Xoorpresa que elijas.
					<br><br>
					En Dulces Xoorpresas creemos que: <br><br>\"Si lo imaginas te ayudamos a hacerlo realidad\".<br><br>";


	$blog = "<h1>Visita nuestro blog en: <br><br>
		<a href='http://dulcesxoorpresas.blogspot.mx/' style='color: #f30050;' ><u>http://dulcesxoorpresas.blogspot.mx/</u></a></h1>";
?>