<?php include 'header.php';
include 'textos.php';
include 'modals.php';
include 'boximg.php';?>
<div class="parallax-container">
    <div class="parallax"><img src="images/251.jpg"></div>
  </div>
  <div class="section white">
    <div class="row">

       <!-- Modal Structure -->
      <?php
      modalM('calaveritas','Calaveritas','Calaveritas pensadas para ofrenda o regalo en Halloween / Dia de muertos.<br>Se pueden personalizar con Nombres costo aproximado de $20');
      ?>

      <ul class="collapsible popout" data-collapsible="accordion">
        <li>
          <div class="collapsible-header"><i class="material-icons">loyalty</i>14 de febrero / Amor<!--i class="material-icons">done</i--></div>
          <div class="collapsible-body">
            <div class="row">
              <?php 
                    //$imagen,$texto en la imagen,$titulo,$idmodal
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$');
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$');
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$');
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$'); ?>
            </div><!--row-->
          </div><!--collapsible-->
        </li><!--lista colapsable-->

        <li>
          <div class="collapsible-header"><i class="material-icons">perm_identity</i>Dia del nino</div>
          <div class="collapsible-body">
            <div class="row">
              <?php 
                    //$imagen,$texto en la imagen,$titulo,$idmodal
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$');
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$');
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$');
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$'); ?>
            </div><!--row-->
          </div><!--collapsible-->
        </li>

        <li>
          <div class="collapsible-header"><i class="material-icons">view_stream</i>15 de septiembre</div>
          <div class="collapsible-body">
            <div class="row">
              <?php 
                    //$imagen,$texto en la imagen,$titulo,$idmodal
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$');
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$');
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$');
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$'); ?>
            </div><!--row-->
          </div><!--collapsible-->
        </li>

        <li>
          <div class="collapsible-header"><i class="material-icons">android</i>Halloween</div>
          <div class="collapsible-body">
            <div class="row">
              <?php 
                    //$imagen,$texto en la imagen,$titulo,$idmodal
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$');
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$');
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$');
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$'); ?>
            </div><!--row-->
          </div><!--collapsible-->
        </li>

        <li>
          <div class="collapsible-header"><i class="material-icons">redeem</i>Navidad</div>
          <div class="collapsible-body">
            <div class="row">
              <?php 
                    //$imagen,$texto en la imagen,$titulo,$idmodal
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$');
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$');
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$');
                    boximg('12.jpg','Calaveritas, precio aproximado $25', 'Calaveritas', 'calaveritas','$$$'); ?>
            </div><!--row-->
          </div><!--collapsible-->
        </li>
      </ul>

    </div>
  </div>
  <div class="parallax-container">
    <div class="parallax"><img src="images/266.jpg"></div>
  </div>
  <?php include 'footer.php';?>

  <script type="text/javascript">
    $(document).ready(function(){
      $('.parallax').parallax();
      $(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();
  });
    });

    
        
  </script>